//
//  Networking.swift
//  putti_apps_ios
//
//  Created by Tony Lin on 31/08/16.
//  Copyright © 2016 Tony Lin. All rights reserved.
//

import Foundation
import SwiftyJSON

private let API_URL = "http://testing.moacreative.com/job_interview/event.php";
private let TIMEOUT:NSTimeInterval = 5;
private let X_KEY = "X-Key"
private let X_KEY_VALUE = "697381b065bbfe4a714cd14cf394978e"
private let USER_AGENT = "MyTestClient : X-Signiture=1Uhi8g9A91"
private let QUERY_STRING = "event_type=interview"

typealias ApiReturnHandler = (json: JSON?, withErr: NSError?)->Void

private let URL_Session : NSURLSession = {
    
    let q = NSOperationQueue()
    q.maxConcurrentOperationCount = 3
    
    return NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration(), delegate: nil, delegateQueue: q)
    
}()

func LoadDataFromApi(ApiReturnHandler handler: ApiReturnHandler) {
    
    //create the instance of NSURL from url string.
    guard let url = NSURL(string : API_URL) else{
        return
    }
    
    //create the Request from URL
    let request = NSMutableURLRequest(URL:url, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: TIMEOUT)
    request.HTTPMethod = "POST"
    request.addValue(USER_AGENT, forHTTPHeaderField: "User-Agent")
    request.addValue(X_KEY_VALUE, forHTTPHeaderField: X_KEY)
    
    //put query string to boddy as the method is post
    if let bodyData = QUERY_STRING.dataUsingEncoding(NSUTF8StringEncoding){
        request.HTTPBody = bodyData
    }
    
    print("API call \(request)")
    
    //call api with apiDeletate then return
    let urlSessionDataTask = URL_Session.dataTaskWithRequest(request, completionHandler: {( data, response, error) in
        
        var json : JSON? = nil
        
        if error == nil { //got response from remote
            if let jsonData = data {
                json = JSON(data : jsonData)
            }
        }else{
            print("err: \(error?.localizedDescription)");
        }
        
        
        handler(json: json, withErr: error)
    })
    
    urlSessionDataTask.resume();
    
}
