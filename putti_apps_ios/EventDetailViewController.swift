//
//  DetailViewController.swift
//  putti_apps_ios
//
//  Created by Tony Lin on 31/08/16.
//  Copyright © 2016 Tony Lin. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

var SelectedIndex : Int?

class EventDetailViewController: UIViewController {

    var Data : EventJson?
    
    @IBOutlet weak var synopsis: UITextView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var aDescription: UIWebView!
    @IBOutlet weak var shareURL: UILabel!
    @IBOutlet weak var mediaURL: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let _index = SelectedIndex else{
            return
        }
        
        let eventJson = EventJsonArray[_index]
        
        self.title = "NewsID:\(eventJson.NewsID)"
        self.synopsis.text = eventJson.Synopsis.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        self.image.loadImageFromURLString(eventJson.ThumbnailImageURL)
        self.startDate.text = "Start Date: \(eventJson.StartDate)"
        self.aDescription.loadHTMLString(eventJson.Description, baseURL: nil)
        self.shareURL.text = "Share URL: \(eventJson.ShareURL)"
        self.mediaURL.text = "Media URL: \(eventJson.MediaURL)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
