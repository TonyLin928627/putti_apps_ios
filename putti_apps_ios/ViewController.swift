//
//  ViewController.swift
//  putti_apps_ios
//
//  Created by Tony Lin on 30/08/16.
//  Copyright © 2016 Tony Lin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var indicator: UIActivityIndicatorView!

    @IBAction func callApi(sender: UIButton) {
        
        sender.enabled = false
        self.indicator.hidden = false;
        
        LoadDataFromApi { (eventJsonArray, error) in
            
            if error != nil {
                self.popAlert(error!.localizedDescription, complition: {
                    sender.enabled = true
                    self.indicator.hidden = true
                })
            }
            
            guard let _eventJsonArray = eventJsonArray else {
        
                self.popAlert("No json data", complition: {
                    sender.enabled = true
                    self.indicator.hidden = true
                })
                
                return
            }
            
            
            EventJsonArray.removeAll()
            
            
            if let eventJsons = _eventJsonArray["Events"].array {
                for _eventJson in eventJsons{
                    let eventJson = EventJson(fromJson: _eventJson)
                    EventJsonArray.append(eventJson)
                }
                self.performSegueWithIdentifier("toTableViewController", sender: self)
            }else{
                self.popAlert("Invalid data format", complition: {
                    sender.enabled = true
                    self.indicator.hidden = true
                })

            }
        }
    }
    
    func popAlert(msg : String, complition: ()->Void){
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler: {alertAction in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: {
            complition()
        })

    }
}

//
//sender.enabled = true
//self.indicator.stopAnimating()
//self.indicator.hidden = true
