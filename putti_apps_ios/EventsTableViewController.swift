//
//  ListViewController.swift
//  putti_apps_ios
//
//  Created by Tony Lin on 31/08/16.
//  Copyright © 2016 Tony Lin. All rights reserved.
//

import UIKit

class EventsTableViewController: UITableViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = "\(EventJsonArray.count) event(s)"
    }
    
    //number of rows
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return EventJsonArray.count
    }
    
    //cell of rows
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("event_item") as! EventTableViewCell
        
        let eventJson = EventJsonArray[indexPath.row]
        
        cell.Data = eventJson
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        print("\(indexPath.row) is selected")
        
        SelectedIndex = indexPath.row
        
    }
    
}

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    
    private var data : EventJson?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var Data : EventJson?{
        get {
            return self.data
        }
        
        set {
            self.data = newValue
            
            self.titleLabel.text = newValue?.Title
            
            self.thumbnail.loadImageFromURLString(newValue?.ThumbnailImageURL ?? "")
            
        }
    }
}
