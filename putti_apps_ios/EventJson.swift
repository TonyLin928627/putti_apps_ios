//
//  EventJson.swift
//  putti_apps_ios
//
//  Created by Tony Lin on 30/08/16.
//  Copyright © 2016 Tony Lin. All rights reserved.
//

import UIKit
import SwiftyJSON

var EventJsonArray = [EventJson]()

struct EventJson {
    
    init (fromJson json: JSON){
        
        self.newsId = json["NewsID"].stringValue
        self.title = json["Title"].stringValue
        self.synopsis  = json["Synopsis"].stringValue
        self.description = json["Description"].stringValue
        self.ordering = json["ordering"].intValue
        self.startDate = json["startDate"].stringValue
        self.eventDateTime = json["eventDateTime"].stringValue
        self.originalImageURL = json["OriginalImageURL"].stringValue
        self.thumbnailImageURL = json["thumbnailImageURL"].stringValue
        self.imageDate = json["ImageDate"].stringValue
        self.shareURL = json["ShareURL"].stringValue
        self.mediaURL = json["MediaURL"].stringValue
        self.updatedDate = json["UpdatedDate"].stringValue
    }

    
    private var newsId: String
    private var title: String
    private var synopsis : String
    private var description: String
    private var ordering : Int
    private var startDate: String
    private var eventDateTime: String
    private var originalImageURL: String
    private var thumbnailImageURL: String
    private var imageDate: String
    private var shareURL: String
    private var mediaURL: String
    private var updatedDate: String
    
    var NewsID : String {
        get{
            return self.newsId
        }
    }
    
    var Title : String{
        get {
            return self.title
        }
    }
    
    var Description: String{
        get {
            return self.description
        }
    }
    
    var Synopsis: String{
        get {
            return self.synopsis
        }
    }

    var Ordering : Int{
        get {
            return self.Ordering
        }
    }

    var StartDate: String{
        get {
            return self.startDate
        }
    }

    var EventDateTime: String{
        get {
            return self.EventDateTime
        }
    }

    var OriginalImageURL: String{
        get {
            return self.originalImageURL
        }
    }

     var ImageDate: String
        {
        get {
            return self.imageDate
        }
    }

    var ShareURL: String{
        get {
            return self.shareURL
        }
    }

    var MediaURL: String{
        get {
            return self.mediaURL
        }
    }

    var UpdatedDate: String{
        get {
            return self.updatedDate
        }
    }

    var ThumbnailImageURL : String{
        get{
            return self.thumbnailImageURL;
        }
    }
    
}






